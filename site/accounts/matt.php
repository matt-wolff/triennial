<?php if(!defined('KIRBY')) exit ?>

username: matt
password: >
  $2a$10$r.3Jm3qyRbSmqC7pxylF1uyra/vMBcHuWNMfVidKXq9aPXrWnUvPu
email: matt.t.wolff@gmail.com
language: en
role: admin
history:
  - acknowledgements
  - authors
  - tuomas-markunpoika
  - foreward
  - preface
