<?php
  $subjects = $site->find('typical-subjects')->children()->shuffle();
?>

<script type="text/javascript">
<?php foreach($subjects as $subject): ?>
  var <?= $subject->title() ?> = {
    Image: {
        xmlns: "http://schemas.microsoft.com/deepzoom/2008",
        Url: "<?= $subject->url() ?>/",
        Format: "jpg",
        Overlap: "0",
        TileSize: "512",
        Size: {
            Width:  "<?= $subject->width() ?>",
            Height: "<?= $subject->height() ?>"
            },
        info: {
            Make: "<?= $subject->make() ?>",
            Location: "<?= $subject->location() ?>",
            Year: "<?= $subject->year() ?>",
            Tags: "<?= $subject->tags() ?>"
        }
        }
  };
<?php endforeach ?>

var tileSources = [
  <?php foreach($subjects as $subject): ?>
    { tileSource: <?= $subject->title() ?>, },
  <?php endforeach ?>
];
</script>
