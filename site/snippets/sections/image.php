<section class="image">

<?php if ($data->picture()->isNotEmpty()): ?>
  <?= $page->image($data->picture()) ?>
<?php endif ?>

  <div class="image-caption">
    <?= $data->text() ?>
  </div>
</section>
