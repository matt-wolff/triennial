<div class="caption none">
  <p class="filename"><?= $image->filename() ?></p>
  <p class="dimensions"><?= $image->dimensions() ?>px</p>
  <?php if ($image->description() != "") : ?>
    <p class="description"><?= $image->description() ?></p>
  <?php endif ?>

  <?php if ($image->source() != "") : ?>
    <p class="source"><a href="<?= $image->source() ?>" target=""><?= $image->source() ?></a></p>
  <?php endif ?>
</div>
