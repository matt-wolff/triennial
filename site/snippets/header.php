<!DOCTYPE html>
<html lang="<?= site()->language() ? site()->language()->code() : 'en' ?>">
<head>
    <meta charset="utf-8">
    <title><?= $site->title()->html() ?> | <?= $page->title()->html() ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?= $site->description()->html() ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    
    <?= js('assets/js/jquery.min.js') ?>
    <?= css('assets/css/fonts.css') ?>
    <?= css('assets/css/main.css') ?>

</head>
<body>
    
<div id="openseadragon1" style="width: 100%; height: 100%; display: block; position: absolute;"></div>    
