<div class="tagging">
  <div class="header">TAGS: </div>
  <ul>
  <?php
    $tags = page('typical-subjects')->children()->pluck('tags', ',', true);
    asort($tags);

    function wrapMe($tag)
    {
      return '<li class="tag">' . $tag . '</li>';
    }

    $tags = array_map("wrapMe", $tags);

    echo implode(' ', $tags);
  ?>
  </ul>
  <br>

  <div class="sorting">
      <div class="header">SORTED BY:</div>
      <ul>
          <li class="sort">Make and Model</li>
          <li class="sort">Location</li>
          <li class="sort">Year</li>
      </ul>
  </div>
  <div class="header" id="all">ALL IMAGES</div>
</div>
