<!DOCTYPE html>
<html lang="<?= site()->language() ? site()->language()->code() : 'en' ?>">
<head>
    <meta charset="utf-8">
    <title><?= $site->title()->html() ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/assets/images/dot.png">
    <link href="/assets/css/lity.css" rel="stylesheet">

    <?= css('assets/css/fonts.css') ?>
    <?= css('assets/css/main.css') ?>
</head>
<body>

  <h1>Oh No!</h1>

<?= js('assets/js/jquery.min.js') ?>
<!-- <?= js('assets/js/lity.js') ?> -->
  <script type="text/javascript">
  $(document).ready(function(){
      console.log(new Date());
  });
  </script>

</body>
</html>
