<!DOCTYPE html>
<html lang="<?= site()->language() ? site()->language()->code() : 'en' ?>">
<head>
    <meta charset="utf-8">
    <title><?= $site->title()->html() ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/png" href="/assets/images/dot.png">

    <?= css('assets/css/fonts.css') ?>
    <?= css('assets/css/main.css') ?>
</head>
<body>
  <nav>
    <a href="/">
    <?php if($image = $site->find('home')->images()->first()): ?>
      <img src="<?php echo $image->url(); ?>"/>
    <?php endif ?>

    <br />
    <?= $site->title()->html() ?>
    <p><?= $page->title() ?></p>
    </a>
  </nav>
<div class="article-wrapper" id="area">
  <article>
    <h1><?= $page->title() ?></h1>
    <?php foreach($page->builder()->toStructure() as $section): ?>
      <?php snippet('sections/' . $section->_fieldset(), array('data' => $section)) ?>
    <?php endforeach ?>
  </article>
</div>
<div class="export">
  <a href="">Generate EPUB</a>
  <a href="">Export to PDF</a>
  <a href="">Share</a>
</div>


<?= js('assets/js/jquery.min.js') ?>
<?= js('assets/js/epub.min.js') ?>
<script type="text/javascript">
$(document).ready(function(){
    console.log(new Date());
});



</script>
</body>
</html>
