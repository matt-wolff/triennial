<!DOCTYPE html>
<html lang="<?= site()->language() ? site()->language()->code() : 'en' ?>">
<head>
    <meta charset="utf-8">
    <title><?= $site->title()->html() ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/png" href="/assets/images/dot.png">

    <?= css('assets/css/fonts.css') ?>
    <?= css('assets/css/main.css') ?>
</head>
<body>
  <nav>
    <?php if($image = $page->images()->first()): ?>
      <img class="" src="<?php echo $image->url(); ?>"/>
    <?php endif ?>
    <br />
    <a class="all"><?= $site->title()->html() ?></a>
    <ul class="menu">
      <li data-category="frontmatter">Front Matter</li>
      <br />
      <li data-category="ethereal">Ethereal</li>
      <li data-category="intricate">Intricate</li>
      <li data-category="transformative">Transformative</li>
      <li data-category="transgressive">Transgressive</li>
      <li data-category="elemental">Elemental</li>
      <li data-category="emergent">Emergent</li>
      <li data-category="extravagant">Extravagant</li>
      <br />
      <li data-category="backmatter">Back Matter</li>
    </ul>
  </nav>
  <ul class="home">
    <?php foreach($site->children()->visible() as $subpage): ?>
    <a href="<?= $subpage->url() ?>" class="thumb <?= $subpage->category() ?>">
      <li>
      <div>
        <?php if($image = $subpage->images()->first()): ?>
          <img src="<?php echo $image->url(); ?>"/>
        <?php else: ?>
          <div class="blank-image"></div>
        <?php endif ?>
          <p class="thumb-caption">
            <span class="category"><?= html($subpage->category()->getCategoryName()) ?>: </span>
            <?= html($subpage->title()) ?>
          </p>
      </div>
      </li>
    </a>

    <?php endforeach ?>
  </ul>

<?= js('assets/js/jquery.min.js') ?>
<script type="text/javascript">
$(document).ready(function(){
    console.log(new Date());
    $(".menu li").click(function(){
      var category = $(this).data("category");
      $(".thumb").hide();
       $( ".thumb" ).each(function() {
        if ($(this).hasClass(category)) {
          $(this).show();
        } else {
          $(this).hide();
        }
    });
    $(".all").click(function() {
      $(".thumb").each(function() {
        $(this).show();
      });
    });
  });
});


</script>
</body>
</html>
