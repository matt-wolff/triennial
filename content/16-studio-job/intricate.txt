Category: intricate

----

Title: Studio Job

----

Builder:

-
  text: 'The work of Sou Fujimoto (Japanese, b. 1971) embraces contrast, harmonizing architecture and nature, public and private, transparency and opacity. Moving to Tokyo from rural Hokkaido to study architecture at Tokyo University, Fujimoto found early inspiration in the tension between the built environment and nature. One can see this in his built work. The Serpentine Pavilion (London, 2013) is a temporary structure of interlocking grids upon which glass planes formed multi- purpose surfaces: steps, seats, tables. The pavilion became landscape through density, blurring into the sky and challenging ideas of space and form. Here Fujimoto demonstrates the dexterity of his craft, creating atmospheric, sensual experiences of space from rigid, austere forms (the grid) and materials (steel). Conceptual projects continue this integration of opposites. Souk Mirage (2013) is a master plan for a city in the Middle East. It proposes stacks of structural arches to comprise an undulating topography of public and private spaces, invigorating the inherent beauty in vernacular Islamic architecture. The grid again generates spatial rhythm, an elegance emerging from complexity, ambiguity paired with order. It is perhaps not surprising to learn that Fujimoto initially studied physics, the most elegant of scientific fields. — ANDREA LIPPS'
  _fieldset: bodytext
-
  text: |
    *What were your early inspirations; what led you to architecture?*

    SOU FUJIMOTO: I was a child who liked to make stuff. I remember flipping through my father’s Gaudí book and seeing pictures of his buildings. That was the ﬁrst time I  came across architecture. In high school, I became interested in physics. Einstein was my role model. It was through reading his theories that I learned  the concept of “space.” I was fascinated by the fact that natural phenomena are gov-erned by physics and, through theories of which, organized and further integrated into clear principles. This has ever since served as the foundation of my architecture practice.

    When I entered college, I decided to pursue architecture as a profession. How-ever, I would say what led to this decision was not determination but rather an inci-dental attempt. I think I was fortunate.

    *Tell me about your current way of working.*

    When I approach a design problem, I often look at several explorations and develop them in parallel. On a sketchbook, I put my ideas into conceptual forms. I also look to my team members for inspirations that can only be obtained through dialogue. Sometimes these dialogues lead to a design approach that is greater than what I would have conceived by myself. Then I set up a few experiments, each with a hypothesis, and observe their growth as I do different tests to them. Physical study  models and computer modeling are also methods I use to explore possibilities, through  which I often ﬁnd new values.

    *How do you engage the user with your architecture?*

    I want people to use and experience my architecture—namely, the building I have designed—in whatever way they want. And because of that, architecture, in my opinion, should not deﬁne how people behave, but rather allow them to develop over time their own relationships with the space in which they inhabit. The pavilion I designed for the Serpentine Gallery in 2013 is a space that brings about interactions between architecture and people.

    *You have described your aesthetic as “primitive future.” Can you elaborate on what this means to you?*

    When we try to conceive architecture for the future, we should look back more often at the fundamentals of architecture: the relationship between body and space, between internal and external, between nature and architecture, between individual and commons. We may ﬁnd new inspirations by revisiting these fundamental ideas.

    *How does beauty factor into your process? And in the final work?*

    When new ideas and concepts form relation-ships with many other things and matters in ways that have never been done before and they become somewhat an integrated whole, that, to me, is beauty. I also ﬁnd a similar kind  of beauty in the clarity of physics and math-ematical formulas.

    *In what ways do you engage with architecture as manifesto?*

    In my architecture practice, I always design with the idea in mind that the architecture- to-be would be a new typology of its kind, be it a house, a library, an apartment, or a tower. And besides creating new typologies, I seek to investigate the integration of architecture with nature.

    *What comes to mind when you hear the word “beauty”?*

    Integration, orchestration, mergence, and clearness.

    *What is the most beautiful time of day?*

    A silent evening.

    *What is the most beautiful place you’ve visited?*

    Marrakesh, Morocco.
  _fieldset: bodytext
-
  text: |
    — INTERVIEW
    WITH ANDREA LIPPS
  _fieldset: endnote
